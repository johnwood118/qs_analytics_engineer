BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "transactions" (
	"trans_id"	TEXT,
	"bar_id"	TEXT,
	"datetime"	TEXT,
	"bar_name"	TEXT,
	"drink_name"	TEXT,
	"price"	REAL,
	"drink_id"	TEXT,
	"glass_type"	TEXT,
	PRIMARY KEY("trans_id")
);
CREATE TABLE IF NOT EXISTS "glass_stock" (
	"glass_id"	TEXT,
	"glass_type"	TEXT,
	"glass_type_sc"	TEXT,
	"bar_name"	TEXT,
	"stock"	INTEGER
);
CREATE TABLE IF NOT EXISTS "drinks" (
	"drink_id"	TEXT,
	"drink_name"	TEXT,
	"glass_id"	TEXT,
	"glass_type"	TEXT
);
COMMIT;
