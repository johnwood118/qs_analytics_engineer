SELECT date(datetime) as date,
t.bar_name,
t.glass_type,
gs.stock,
count(DISTINCT trans_id) as trans_cnt
FROM transactions as t
LEFT JOIN glass_stock AS gs 
ON (t.bar_name = gs.bar_name 
AND t.glass_type = gs.glass_type_sc)
GROUP BY date, t.bar_name, t.glass_type