# Glass Management POC Database Build

### Requirements
To run this build you will need:
- SQLite editor such as DB Browser Lite. Available to download for free.
- A Programme to run Python scripts (.py), such as Pycharm or Spyder.

### Description of Contents 
Inside the folder provided you should find the following: 

- `raw_data` folder - This folder contains the raw files you provided, including Glass stock data and Transaction data 
for each bar.
- `curated_data` folder - This folder will contain various `.csv` files of the transformed and 'cleaned' data you provided. 
These are like-for-like tables that you can find in the SQLite Database that will be built. This folder will be empty until you run `build_db.py`.
- `build_db.py` - This is the main python script which will conduct the cleaning of raw files, and build the SQLite 
Database.
- `create_qs_bar_database.sql` - This is the SQLite script that will be used to create the 3 tables for the SQL Database.
- `manage_glass_stock.sql` - This is the SQLite script which contains a query to begin exploration of daily transactions 
and glass stock for all bars.
- `Questions and Plans.doc` - This is a MS Word document which contains possible data exploration ideas to obtain 
insightful information from the `qs_bar_database.db`.

### Description of Outputs
After running the `build_db.py` script, you will find the following outputs:
- `glass_stock.csv`, `transactions.csv` and `drinks.csv` inside the `curated_data`.
- `qs_bar_database.db` - The main database containing 3 tables for exploration. Open this using an SQLite Editor.

### Steps to run 
To receive outputs and begin data exploration, run the `build_db.py` script in a Python IDE or Console.
Inside your SQLite Editor such as DB Browser Lite, you will then be able to open the `qs_bar_database.db`.
Run the `manage_glass_stock.sql` file from inside the SQL editor, or open using notepad; copy and paste the query into
the editor.

### Future work

To conduct data analysis on the database, you will find some potential questions to explore on the `qs_bar_database.db`. 
All of which can be answered with the data provided, given the correct SQLite knowledge. 

If you require to add more transaction data, then you need to edit **Section 2** in the `build_db.py` script. 
Ensure that the transaction data you provide contains dates using the format YYYY-MM-DD and contains *only* the following
columns:
- Date and Timestamp
- Drink Name
- Price

