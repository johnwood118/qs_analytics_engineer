'''
The Following script is broken into three parts:
1. Create 'Drinks' table
2. Process and Curate Transaction Data
3. Create and Load tables into SQLite Database


Section 1: Create Drinks Table
The Purpose of this Section is to create a Drinks Dataframe which contains both
drink names and glass names. This can be used as a link between Transaction data
and Glass Stock data.

To retrieve data for drinks and their associated glass types, an api will be called from:
https://www.thecocktaildb.com/api.php

Each Glass type has its own database of drinks via a unique api link.
The 'glass_type' variable in the Glass stock data [bar_data.csv] will be used to
retrieve the correct API link.

The final [drinks_df.csv] will be exported to the bucket 'curated_data'.
[bar_data.csv] will also be cleaned and exported as ['glass_stock.csv] to 'curated_data'

'''

## Load Required Libraries ----

import pandas as pd
import requests
import json
import glob
import re
import sys
import hashlib
import sqlite3


## Set Parameters and Constants ----

# File paths
in_path = r'raw_data'
out_path = r'curated_data'
filenames = glob.glob(in_path + "/*")

# Read API
# Global URL
gbl_url = 'http://www.thecocktaildb.com/api/json/v1/1/'

# URL extention functions
fltr_url_key = 'filter.php?'
lst_url_key = 'list.php?'


## Load Data ----

# Load Bar Data
bar_df = pd.read_csv(f'{in_path}/bar_data.csv')

# Load Glass Type Data
# Extract list of Glass Types from API
response = requests.get(f'{gbl_url}{lst_url_key}g=list')

# Extract the response text as json object
glass_json = json.loads(response.text)

# Extract the Drinks Dictionary List
glass_dct_lst = glass_json['drinks']
# print(drinks)

# Get Drink name and ID from List of Dictionaries
glass_list = list(map(lambda key: [key.get("strGlass")], glass_dct_lst))

# Create dataframe from Extracted list of Keys
glass_df = pd.DataFrame(glass_list, columns=['glass_name'])


## Feature Engineering ----

# Clean Bar Data
# Create secondary Glass type Column for future merging to api data
bar_df['glass_type_sc'] = bar_df['glass_type'].str.replace(' ', '_')

# Change 'bar' to snake_case for future merging to transaction data
bar_df['bar_name'] = bar_df['bar'].str.replace(' ', '_')
# Drop old column
del bar_df['bar']

# Remove Characters from Stock Column
bar_df['stock'] = bar_df['stock'].str.replace(r'[^0-9]', '')

# Set 'Stock' column to integer
bar_df['stock'] = pd.to_numeric(bar_df['stock'])

# Create Unique Identifier for each glass type using Glass name
bar_df['glass_id'] = [hashlib.md5(val.encode('UTF-8')).hexdigest() for val in bar_df['glass_type_sc']]

# Clean Glass Data
# Replace whitespace with underscore ready for api input
glass_df['glass_api_name'] = glass_df['glass_name'].str.replace(' ', '_')

# Create tmp column for merging with bar data
glass_df['glass_type_sc'] = glass_df['glass_api_name'].str.lower()

# Extract Glass Types
glass_typ_lst = glass_df['glass_api_name'].to_list()

# Create Empty Drinks Dataframe ready to fill
drinks_df = pd.DataFrame({'drink_id': pd.Series(dtype='str'),
                          'drink_name': pd.Series(dtype='str'),
                          'glass_type': pd.Series(dtype='str')
                          })

# Create Function to extract Drink names from API using Glass types
def extract_drinks_db(gls_typs):
    '''
    # For each glass type name we need to:
    # 1. Extract the relevant api
    # 2. Extract the associated drink names and IDs
    # 3. Convert Extracts to a DF
    # 4. Add glass name to the DF
    # 5. Append results to drinks_df

    :param gls_typs:
    :return: drinks_df
    '''

    for gt in gls_typs:

        # Set API link: use 'g=' to filter by glass
        api_url = f'{gbl_url}{fltr_url_key}g={gt}'
        print(f'Retrieving data from: {api_url}')

        # Get the glass type api
        rpnse = requests.get(api_url)

        # Exceptions need to be made if a glass type cannot be found as an API
        if rpnse.text == '':
            # Raise exception error
            print(f"Failed to receive response from API for glass type: {gt}. Check api input.")
            pass
        else:
            # Extract the response text
            rpnse_data = rpnse.text

            # Convert data to Json
            rpnse_json = json.loads(rpnse_data)

            # Extract the Drinks Dictionary List
            drinks_dct_lst = rpnse_json['drinks']
            # print(drinks)

            # Get Drink name and ID from List of Dictionaries
            extract_list = list(map(lambda key: [key.get("idDrink"), key.get("strDrink")], drinks_dct_lst))
            # print(extract_list)

            # Create dataframe from Extracted list of Keys
            df = pd.DataFrame(extract_list, columns=['drink_id', 'drink_name'])

            # Convert to drink name to lower snakecase for merging with Glass Stock Data
            df['drink_name'] = df['drink_name'].str.replace(' ', '_').str.lower()

            # Add Glass Type to DF
            df['glass_type'] = f'{gt}'

            # Convert to Glass Type to snakecase for merging with Glass Stock Data
            df['glass_type'] = df['glass_type'].str.replace(' ', '_').str.lower()

            # print(df)
            # print(df.dtypes)

            # Append to main DF
            global drinks_df

            drinks_df = pd.concat([drinks_df,
                                   df], axis=0)

# Run function to Create drinks dataframe
extract_drinks_db(glass_typ_lst)
print(drinks_df)

# Create Unique Identifier for each glass type using Glass name
drinks_df['glass_id'] = [hashlib.md5(val.encode('UTF-8')).hexdigest() for val in drinks_df['glass_type']]

# Order columns before saving
# Bar Stock df
col_order = ['glass_id',
             'glass_type',
             'glass_type_sc',
             'bar_name',
             'stock']

bar_df = bar_df[col_order]

# Drinks DF
col_order = ['drink_id',
             'drink_name',
             'glass_id',
             'glass_type']

drinks_df = drinks_df[col_order]

# Export Bar df and Drinks DF
drinks_df.to_csv(f'{out_path}/drinks_df.csv', index=False)
bar_df.to_csv(f'{out_path}/glass_stock.csv', index=False)


'''
Section 2: Process and Curate Transaction Data

This section will import the transaction data from each bar, clean and standarise them
and finally combine into a single transaction table called 'transactions_df.

This will be exported to the curated folder under the name 'transactions_df.csv'

The transaction data can be used in conjunction with 'glass_stock' to identify
if transactions in a single day have exceeded glass stock in a given bar.

'''

# Load London Data
ldn_trans = pd.read_csv(f'{in_path}/london_transactions.csv.gz',
                        compression = 'gzip',
                        delimiter= r"\t",
                        engine ='python')

# Load Budapest Transaction Data
bdpst_trans = pd.read_csv(f'{in_path}/budapest.csv.gz',
                          compression = 'gzip')

# Load NY Transaction Data
ny_trans = pd.read_csv(f'{in_path}/ny.csv.gz',
                        compression = 'gzip')

# Load Drinks_df
drinks_df = pd.read_csv(f'{out_path}/drinks_df.csv')

# Rename Columns
# Set Desired Column Names
col_names = ["index", "datetime", "drink_name", "price"]

ldn_trans.columns = col_names
bdpst_trans.columns = col_names
ny_trans.columns = col_names

# Drop index
del ldn_trans['index']
del bdpst_trans['index']
del ny_trans['index']

# # List Dataframes to parse
# df_list = [ldn_trans, bdpst_trans, ny_trans]

# Convert date time columns to datatype datetime
ldn_trans['datetime'] = pd.to_datetime(ldn_trans['datetime'],
                                       format='%Y-%m-%d %H:%M:%S')

bdpst_trans['datetime'] = pd.to_datetime(bdpst_trans['datetime'],
                                         format='%Y-%m-%d %H:%M:%S')

ny_trans['datetime'] = pd.to_datetime(ny_trans['datetime'],
                                      format='%m-%d-%Y %H:%M')

# Add Column to indicate region
ldn_trans['bar_name'] = 'london'
ny_trans['bar_name'] = 'new_york'
bdpst_trans['bar_name'] = 'budapest'

# Ensure other columns conform to expected datatype
# Use dictionary to convert specific columns

convert_dict = {'bar_name': str,
                'drink_name': str,
                'price': float
                }

# Convert to correct types
for df in [ldn_trans, bdpst_trans, ny_trans]:
    df = df.astype(convert_dict)
    print(df.dtypes)


# Order Columns for all tables ready for Concatenation
# London does not need reordering

col_order = ['datetime',
             'bar_name',
             'drink_name',
             'price']

ldn_trans = ldn_trans[col_order]
ny_trans = ny_trans[col_order]
bdpst_trans = bdpst_trans[col_order]

# Concatenate Dataframes to create a single Transaction Table
trans_df = pd.concat([ldn_trans,
                      ny_trans,
                      bdpst_trans], axis=0)

# Create Trans ID equal to number of rows
trans_df = trans_df.reset_index(drop=True)
trans_df['trans_id'] = trans_df.index + 1

# Create Region ID
# Create ID based on Region
# Create Unique Identifier for each Bar type using Bar name
trans_df['bar_id'] = [hashlib.md5(val.encode('UTF-8')).hexdigest() for val in trans_df['bar_name']]

# Reorder Columns
col_order = ['trans_id',
             'bar_id',
             'datetime',
             'bar_name',
             'drink_name',
             'price']

trans_df = trans_df[col_order]

# Check drink names

# strip non-alphanumeric, whitespace and convert to lower case for drink names
# Note: Doesn't deal with accented characters
trans_df['drink_name'] = trans_df['drink_name'].str.replace(' ', '_').str.lower()

# Check if there was a consolidation of drink names via text transformation
trans_df['drink_name'].nunique()

# Quick aggregate of unique drink names
drink_count = trans_df.groupby('drink_name')['trans_id'].nunique()

# Randomly select rows for check
trans_df.sample(n=10)
drink_count.sample(n=10)

# Add Drink ID and Glass Name to Transaction DF using drink name

merged_df = pd.merge(trans_df,
                     drinks_df,
                     how = 'left',
                     on = 'drink_name')

# Random Check
merged_df.sample(n=10)

# How many unique values of each variable has no drink id
merged_df[merged_df['drink_id'].isnull()].nunique()

# Extract Null Drink ID DF
null_drink = merged_df[merged_df['drink_id'].isnull()]
null_drink.groupby('drink_name')['trans_id'].nunique()

# Export transaction df
merged_df.to_csv(f'{out_path}/tranactions_df.csv', index=False)

'''
Section 3: Create and Load tables into SQLite Database

This section takes each curated table and creates a table for each in an sqlite database.
First the database is created, if it doesn't already exist, runs an sql query to create each
table, and finally uses the pandas dataframe to export the table into the corresponding sql-table

'''

# Set up connection to the sqlite database
conn = sqlite3.connect('qs_bar_database.db')

# Create Cursor
c = conn.cursor()

# Query to Create Transaction Table
trans_tbl_qry = """
CREATE TABLE IF NOT EXISTS 'transactions' (
	'trans_id'	TEXT,
	'bar_id'	TEXT,
	'datetime'	TEXT,
	'bar_name'	TEXT,
	'drink_name'	TEXT,
	'price'	REAL,
	'drink_id'	TEXT,
	'glass_type'	TEXT,
	PRIMARY KEY('trans_id')
)
"""

# Query to Create Glass Stock Table
glass_tbl_qry = """
CREATE TABLE IF NOT EXISTS "glass_stock" (
	"glass_id"	TEXT,
	"glass_type"	TEXT,
	"glass_type_sc"	TEXT,
	"bar_name"	TEXT,
	"stock"	INTEGER
)
"""

# Query to Create Drinks Table

drinks_tbl_qry = """
CREATE TABLE IF NOT EXISTS "drinks" (
	"drink_id"	TEXT,
	"drink_name"	TEXT,
	"glass_id"	TEXT,
	"glass_type"	TEXT
)
"""

# Execute Queries
c.execute(trans_tbl_qry)
c.execute(glass_tbl_qry)
c.execute(drinks_tbl_qry)

# Insert values into database
# Fill transaction DB
merged_df.to_sql('transactions',
                 conn,
                 if_exists = 'replace',
                 index=False)

# Fill Glass Stock DB
bar_df.to_sql('glass_stock',
                 conn,
                 if_exists = 'replace',
                 index=False)

# Fill Drinks DB
drinks_df.to_sql('drinks',
                 conn,
                 if_exists = 'replace',
                 index=False)


# Commit Inserts and Close
conn.commit()
conn.close()
